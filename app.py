from flask import Flask, render_template, request, flash, session, url_for, redirect, g
import os
from flask_bootstrap import Bootstrap
from flask_wtf import Form
from wtforms import StringField, TextField, SubmitField, validators, ValidationError, PasswordField
from wtforms.validators import DataRequired, Length
#from flask.ext.sqlalchemy import SQLAlchemy
import sqlite3


app = Flask(__name__)

 #config
app.secret_key = 'my precious'
app.database = 'openexport.db'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///Heta.db'
Bootstrap(app)
#db = SQLAlchemy(app)




@app.route('/')
def view_form():
    return render_template('index.html')
        
@app.route('/export', methods=['GET', 'POST'])
def export():
    g.db = connect_db()
    #cur = g.db.execute('select * from product')
    cur = g.db.execute('select * from doorStandards')
    #product = [dict(productName = row[0], currentCountry = row[1], countryExportingTo= row[2], standards = row[3]) for row in cur.fetchall()]
    doorStandards = [dict(Id = row[0], Security = row[1], safetyFire= row[2], safetyInUse = row[3]) for row in cur.fetchall()]
    g.db.close()
    return render_template('answer.html', doorStandards = doorStandards)


    # if request.method == 'POST':
    #     if request.form['product'] == 'door':
    #         if request.form['countryProducing'] == 'uk':
    #             if request.form['countryExportingTo'] == 'germany':
    #                 return redirect(url_for('export'))
    # return render_template('answer.html', product = product, doorStandards = doorStandards)

    



# connect to database
def connect_db():
      return sqlite3.connect('openexport.db')


if __name__ == '__main__':
    app.run(port=8080, host='0.0.0.0', debug=True)


