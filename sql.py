import sqlite3

Security = """Security doorsets form part of an integrated site security system. It is important that their
performance is considered in conjunction with other aspects of site security such as CCTV, detection
systems, manned guarding and security procedures. For further guidance contact CPNI."""

safetyFire = """The Regulatory Reform (Fire Safety) Order 2005 or FSO, replaced over 70 pieces of fire safety law and came into force in 2006. The responsibility for fire risk assessment in all non-domestic buildings, including the common parts of flats and houses of multiple occupation, falls to the so-called 'responsible person'. Under the FSO, the responsible person must carry out a fire safety risk assessment and implement and maintain a fire management plan. Further information on what you need to do when carrying out a risk assessment is available here."""

safetyInUse = """ If a residential installation is found to be dangerous to
the general public due to its design, location and
condition (e.g: accessible from a public right of way or
road and unsafe), then the installer should again notify
the client in writing that the gate should be taken out of
action. It is possible that, if the client left the dangerous
gate in operation, this could result in a civil claim resulting
from any accident occurring on that gate. """

Id = "BS 8529:2010"




with sqlite3.connect("openexport.db") as connection:
	c = connection.cursor()

	c.execute("CREATE TABLE doorStandards(Id VARCHAR PRIMARY KEY, Security TEXT, safetyFire TEXT, safetyInUse TEXT )")
	c.execute("CREATE TABLE product(productName VARCHAR, currentCountry TEXT, countryExportingTo TEXT, standards VARCHAR, FOREIGN KEY(standards) REFERENCES doorStandards(Id) )")
	c.execute('INSERT INTO product(productName, currentCountry, countryExportingTo, standards) VALUES ("Door", "Britain", "Germany", "BS 8529:2010")')
	c.execute("INSERT INTO doorStandards(Id, Security, safetyFire, safetyInUse) VALUES (?,?,?,?)", (Id, Security, safetyFire, safetyInUse))